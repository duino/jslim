var requirejs = require('requirejs');
// var assert = require('assert');
requirejs.config({
  //Pass the top-level main.js/index.js require
  //function to requirejs so that node modules
  //are loaded relative to the top-level JS file.
  nodeRequire: require
});

requirejs(['../jslim'],
  function(slim) {
    //foo and bar are loaded according to requirejs
    //config, but if not found, then node's require
    //is used to load the module.
    var date = new Date();
    var cdate = slim.compress(date);
    console.log(cdate.length);
    var udate = slim.decompress(cdate);
    console.log(udate.length);

    var jobject = {
      text : 'I am just an ordinary object',
      array : ['with','an', 'array', 1,2,3],
      object : {
        one: 'one',
        two: 2,
        now: 11002238949.12313
      }
    };

    var str = JSON.stringify(jobject);
    var slm = slim.compress(jobject);
    console.log('uncompressed:'+str.length);
    console.log('comppressed:'+slm.length);
    var ob1 = slim.decompress(slm);
    if(ob1.text !== jobject.text){
      console.log('error');
    }

  }
);
