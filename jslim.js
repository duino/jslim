/**
 * JSON-SLIM / jslim.js
 * Is a small utility to replace JSON for local storage purposes
 * It's packing double asci chars(utf-8) into a single UTF-16 position to save space
 * There is no advantage to replace JSON when sending the data over a HTTP connection
 */

if (typeof define !== 'function') {
  // nodejs implementation
  var define = require('amdefine')(module); // eslint-disable-line
}
define([], function () {
  'use strict';

  /**
   * Determine a javascript var type
   * @param  {any} fn
   */
  const jsType = function(fn){
    if (typeof fn === 'undefined') {
      return 'undefined';
    }
    return ({}).toString.call(fn).match(/\s([a-z|A-Z]+)/)[1];
  };

  const escapeHtml= function(v) {
    if(!v){
      return '';
    }
    v += ''; // cast to string
    v = doUnescape(v);
    var htmlEscapes = {
      '&': '&amp;',
      '<': '&lt;',
      '>': '&gt;',
      '"': '&quot;',
      "'": '&#x27;',  //eslint-disable-line
      '/': '&#x2F;'
    };
    var htmlEscaper = /[&<>"'\/]/g;
    return v.replace(htmlEscaper, function(match){
      return htmlEscapes[match];
    });
  }

  const escapeHash = {
    _ : function(input) {
      var ret = escapeHash[input];
      if(!ret) {
        if(input.length - 1) {
          ret = String.fromCharCode(input.substring(input.length - 3 ? 2 : 1));
        }
        else {
          var code = input.charCodeAt(0);
          ret = code < 256
            ? '%' + (0 + code.toString(16)).slice(-2).toUpperCase()
            : '%u' + ('000' + code.toString(16)).slice(-4).toUpperCase();
        }
        escapeHash[ret] = input;
        escapeHash[input] = ret;
      }
      return ret;
    }
  };

  const doEscape = function(str){
    // escape is deprecated
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/escape
    if(escape){
      return escape(str);
    }
    // Fallback escape function
    // got this from here
    // http://cwestblog.com/2011/05/23/escape-unescape-deprecated/
    return str.replace(/[^\w @\*\-\+\.\/]/g, function(aChar) {
      return escapeHash._(aChar);
    });
  };

  const doUnescape = function(str){
    if(unescape){
      return unescape(str);
    }
    return str.replace(/%(u[\da-f]{4}|[\da-f]{2})/gi, function(seq) {
      return escapeHash._(seq);
    });
  };

  /**
   * compress
   *
   * @param  {any} s
   * @returns {string}
   */
  const compress = function (s) {
    s = JSON.stringify(s);
    var out = '', i, len, val;
    len = s.length;
    if (len < 1 || typeof s !== 'string') {
      return s;
    }
    // Ensure 1 byte chars (0 / 254)
    s = doUnescape(encodeURIComponent(s));
    if ((len % 2) === 1) {
      // Ad an extra byte for byte allignment
      // Odd bytes won't fill a 16 bits slot
      s += String.fromCharCode(0);
    }
    i = 0;
    len = s.length;
    for (; i < len; i += 2) {
      val = (s.charCodeAt(i + 1) << 8) + (s.charCodeAt(i));
      if (val > 0) {
        out += String.fromCharCode(val);
      }
    }
    return out;
  };

  /**
   * decompress
   *
   * @param  {string} s
   * @returns {string}
   */
  const decompress = function (s) {
    var res;
    if (typeof s !== 'string' || s.length < 1) {
      return s;
    }
    var n, out = '', high, low, i = 0, len = s.length;
    for (; i < len; i++) {
      n = s.charCodeAt(i);
      high = n >> 8;
      low = n - (high << 8);
      out += String.fromCharCode(low);
      if (i === len - 1 && high === 0) {
        // Skip byte
      } else {
        out += String.fromCharCode(high);
      }
    }
    try {
      res = JSON.parse(decodeURIComponent(doEscape(out)));
    } catch(e){
      // nothing
    }
    return res;
  };

  // expose the public api
  return {
    compress:compress,
    decompress:decompress,
    escape: doEscape,
    unescape: doUnescape,
    escapeHtml : escapeHtml,
    type:jsType
  };
});
