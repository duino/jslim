# jslim

Compression for Javascript String Object Notation (json).
Almost doubles the storage on 'devices' with a javascript environment when
storing values in a latin character set.


It requires requirejs in nodejs or in a browser.

```
npm install jslim --save
```


```
/* UNCOMMENT THIS IN A NODEJS ENVIRONMENT */
// var requirejs = require('requirejs');
// requirejs.config({
//   nodeRequire: require
// });

requirejs(['jslim'],
  function(slim) {
    var date = new Date();
    var cdate = slim.compress(date);
    console.log(cdate.length);
    var udate = slim.decompress(cdate);
    console.log(udate.length);

    var jobject = {
      text : 'I am just an ordinary object',
      array : ['with','an', 'array', 1,2,3],
      object : {
        one: 'one',
        two: 2,
        now: 11002238949.12313
      }
    };

    var str = JSON.stringify(jobject);
    var slm = slim.compress(jobject);
    console.log('uncompressed:'+str.length);
    console.log('comppressed:'+slm.length);
    var ob1 = slim.decompress(slm);
    if(ob1.text !== jobject.text){
      console.log('error');
    }

  }
);

```
